%!TEX root=../thesis.tex

\emph{Visual analytics} stands as a cooperative data analysis paradigm that aims to find effective ways of combining analytical power from humans and computers  \cite{Keim2008}.

When data include information about location and time, it is usually called \emph{spatiotemporal data}. 
Spatiotemporal data may be represented as \emph{spatial time series}. 
Analysis of spatial time series is not always straightforward.
% desarrollar spatiotemporal data
For instance, identifying regions and time
intervals of abrupt and/or smooth variation on spatial time series, while
visually tracking the evolution of those patterns over time, is not an
easy task. 
A sudden temporal variation can indicate the appearance of an unexpected
event or reveal a change in the behavior of some
phenomenon. 
Furthermore, analyzing variations over time helps in understanding its temporal evolution.  
Therefore, providing users with effective tools for analyzing spatial time series
is a challenge.


The literature about time-varying data visualization is extensive and comprehensive surveys can be found in the works by~\cite{Aigner2007,Aigner2011}, which develop a systematic review on time-varying data visualization.
Another review presented by \cite{Bach2014} classifies time-varying data visualizations based on space-time cube operations.
The related problem of movement visualization was also extensively reviewed \cite{Demsar2015,Andrienko2013}.



%From the spatial perspective, \cite{DRST2014} presents a systematic review with respect to the dimensionality of attributes space and reference space.


%and~\cite{Kehrer2013}, which discusses visual analysis methods for multifaceted data. 
%While those surveys are not focused on visualization of functions/signals on graphs, they can provide insights about spatio-temporal methods. 
%The visualization of dynamic graphs~\cite{Beck2014} is also a related problem, but with different objectives.
   
\section{Visualization of spatial time-series}


\subsection{Integrated views}

\begin{figure}
\begin{tikzpicture}
\node[inner sep=0,text width=.4\linewidth,align = center] (saito2005) at (-7,1)
    {\includegraphics[width=.75\linewidth]{figs/related/2D/saito2005_diag}\\ 
    \includegraphics[trim = 3cm 1mm 0 0,clip=true,width=\linewidth]{figs/related/2D/saito2005} 
     \textcolor{mygreen}{A} };

\node[inner sep=0,text width=.45\linewidth,align = center] (b) at (0,3)
    {\includegraphics[width=\linewidth]{figs/related/2D/Andrienko2004} 
     \textcolor{mygreen}{B}};
    
\node[inner sep=0,text width=.4\linewidth,align = center] (c) at (0,-2)
    {\includegraphics[width=\linewidth]{figs/related/2D/shanbhag2005} 
     \textcolor{mygreen}{C}};

\node[inner sep=0,text width=.85\linewidth,align = center] (andrienko2011) at (-3.4,-7.5)
    {\includegraphics[trim = 0 0 0 2.5cm,clip=true,width=\linewidth]{figs/related/2D/andrienko2011}
     \textcolor{mygreen}{D}};

\node[inner sep=0,text width=.98\linewidth,align = center] (shrestha2013) at (-3.4,-12)
    {\includegraphics[width=.27\linewidth]{figs/related/2D/shrestha2013_diag}\quad
    \includegraphics[width=.67\linewidth]{figs/related/2D/shrestha2013}\\ 
     \textcolor{mygreen}{E}};

\end{tikzpicture}
{\phantomsubcaption{\label{fig:i2d_a}}
  \phantomsubcaption{\label{fig:i2d_b}}
  \phantomsubcaption{\label{fig:i2d_c}}
  \phantomsubcaption{\label{fig:i2d_d}}
  \phantomsubcaption{\label{fig:i2d_e}}
}
\caption[Time series integrated views in 2D.]{Time series integrated views in 2D. 
\subref{fig:i2d_a} Two-tone pseudo coloring technique \cite{Saito2005}.
\subref{fig:i2d_b} Value flow maps \cite{Andrienko2004}.
\subref{fig:i2d_c} Time-oriented polygons \cite{Shanbhag2005}.
\subref{fig:i2d_d} Growth Ring Maps \cite{Bak2009,Andrienko2011}. 
\subref{fig:i2d_e} Storygraphs \cite{Shrestha2013}.
}
\label{fig:integrated2D}
\end{figure}

Integrated views attempt to reduce cognitive load on users by presenting the information in a single view.
When visualizing spatial time series, time series glyphs \cite{Fuchs2013} can be plotted on top of a spatial map.
To avoid clutter, such glyphs must be capable of encoding large time series in a space-efficient way \cite{Wickham2012}. For instance,~\citeonline{Saito2005} encode temperature time series using the two-tone pseudo coloring technique, resulting in a compact glyph which is later plotted over a geographic map, as depicted in \Cref{fig:i2d_a}.
In a similar manner, \emph{Value Flow Maps}  \cite{Andrienko2004}, representing the temporal behavior of an area, were plotted over its corresponding area on a cartographic map  (\Cref{fig:i2d_b}).

Another possibility is to use the entire spatial area associated to the time series to display a time graph. Such approach was called \emph{time-oriented polygons} by \cite{Shanbhag2005}.
An example of the representation of an increasing and decreasing time series is shown in \Cref{fig:i2d_c}.

Pixel oriented graphs \cite{Keim2000} can also be plotted over a map to display spatial time series. 
\cite{Bak2009} propose to represent every spatiotemporal event by one pixel placed in an orbital way. Colors can be used to represent categories or periods of time. Such representation is called \emph{Growth Rings} and has been used by \cite{Andrienko2011} to display the number of photos taken at different locations   (\Cref{fig:i2d_d}).
The advantage of pixel oriented graphs over glyphs and time-oriented polygons is that they are highly scalable for large time series.
However, when dealing with spatially dense time series, spatial clutter is a drawback for both approaches. 

A quite different approach, illustrated in \Cref{fig:i2d_e}, is proposed by \cite{Shrestha2013}. 
There, a graph with two vertical axes is used to represent spatial dimensions, e.g. latitude and longitude, while time is represented in the horizontal axis. 
So, each latitude-longitude coordinate is represented by a line connecting its values at each respective axis. 
Temporal events occurring at certain coordinate are plotted through its corresponding line resulting in the so called \emph{Storygraphs}.
Those graphs can be augmented with frequency information reflected as color intensity \cite{Li2014}.
While this approach can display large amounts of spatiotemporal data, spatial proximity of events can go unnoticed.

%3D
\begin{figure}
\begin{tikzpicture}
\node[inner sep=0,text width=.3\linewidth,align = center] (tominski) at (0,1)
    {\includegraphics[trim = 3cm 1mm 0 0,clip=true,width=\linewidth]{figs/related/3D/tominski}  \textcolor{mygreen}{A}};
    
\node[inner sep=0,text width=.62\linewidth,align = center] (thakur) at (.5\linewidth,1)
    {\includegraphics[trim = 0 0 0 0,clip=true,width=\linewidth]{figs/related/3D/thakur}\\  \textcolor{mygreen}{B}};
    
\node[inner sep=0,text width=\linewidth,align = center] (cheng) at (.3\linewidth,-5.1){
   \includegraphics[trim = 0 0 0 0,clip=true,width=.23\linewidth]{figs/related/3D/cheng_iso} \quad
   \includegraphics[trim = 0 0 0 0,clip=true,width=.3\linewidth]{figs/related/3D/cheng_constrained} \;
   \includegraphics[trim = 0 0 0 0,clip=true,width=.33\linewidth]{figs/related/3D/cheng_wall} 
    \\[.1cm]  \textcolor{mygreen}{C}};
\end{tikzpicture}
{\phantomsubcaption{\label{fig:i3d_a}}
  \phantomsubcaption{\label{fig:i3d_b}}
  \phantomsubcaption{\label{fig:i3d_c}}
}
\label{fig:integrated3D}
\caption[Time series integrated views in 3D.]{
Integrated views in 3D.
\subref{fig:i3d_a} 3D icons on a map \cite{Tominski2005}.
\subref{fig:i3d_b} Data Vases \cite{Thakur2009}.
\subref{fig:i3d_c} Isosurfaces, constrained isosurfaces and 3D wall maps, respectively \cite{Cheng2013}.
}
\end{figure}

Displaying temporal information over a spatial map has also been approached using 3D views. The \emph{space-time cube metaphor} considers time as an additional dimension for plotting spatiotemporal data. 
%The base of the cube represents the two geographical spatial dimensions while time is represented in the height of the cube as a third dimension.
Including time as a third dimension was first proposed by \cite{Hagerstraand1970}. 
This approach was then extensively used to visualize spatiotemporal data, such as earthquakes \cite{Gatalsky2004}, precipitating clouds \cite{Turdukulov2007}, space-time paths \cite{Lenntorp1976,Kraak2003}, stock movements \cite{Dwyer2004}, among others.

\cite{Tominski2005} display time series as 3D icons on a map (\Cref{fig:i3d_a}).
To avoid cluttering, they allow users to hide information by filtering attributes on each time slice.
Colors are used to display the maximum value at each time, which they call maxima-events.
Time series are also represented as 3D columns called \emph{Data Vases} \cite{Thakur2009,Thakur2010}, 
where time slices are considered to have a fixed height while the column width represents some attribute or statistical property.
This approach is illustrated in \Cref{fig:i3d_b}.
Manipulation of the 3D view and interactive techniques, such as brushing and filtering, are allowed. 
Besides, aggregation is used as a data transformation method to alleviate cluttering.

From data obtained at traffic sensors, \cite{Cheng2013} used the time-space cube metaphor to visualize traffic congestion.
Isosurfaces, isosurfaces constrained to the road network and 3D wall maps are constructed from spatial time series. An example of each of these appears in \Cref{fig:i3d_c}.
In addition to visual elements, no further interaction is addressed in such work.

Data occlusion and spatial cluttering, along with 3D interaction difficulties are some of the common drawbacks of techniques using the space-time cube metaphor.
Besides, these techniques usually need to be combined with more specialized data analysis methods to allow users to make sense of data.


\subsection{Multiple views}

\begin{figure}[h!]
\begin{tikzpicture}
\node[inner sep=0,text width=\linewidth,align = center] (usdrought) at (0,0)
    {\includegraphics[trim = 3cm 1mm 0 0,clip=true,width=\linewidth]{figs/related/multiple/usdrought}  \textcolor{mygreen}{A}};
    
\node[inner sep=0,text width=\linewidth,align = center] (ferreira) at (0,-6.6)
    {\includegraphics[trim = 0 0 0 0,clip=true,height=.4\linewidth]{figs/related/multiple/ferreira}\qquad
    \includegraphics[trim = 0 0 0 0,clip=true,height=.4\linewidth]{figs/related/multiple/jern}
    \\  \textcolor{mygreen}{B}};
  
\end{tikzpicture}
{\phantomsubcaption{\label{fig:mult_a}}
  \phantomsubcaption{\label{fig:mult_b}}
}
\label{fig:integrated3D}
\caption[Time series multiple views.]{
Multiple views.
\subref{fig:i3d_a} Small multiples \cite{nytimesSm}.
\subref{fig:i3d_b} Coordinated multiple views: Taxivis  \cite{Ferreira2013} and GeoAnalytics \cite{Jern2006}, respectively.
}
\end{figure}



Each time slice of spatiotemporal data can be represented by a spatial graph, and
several of such spatial graphs can be displayed together in a \emph{Small Multiples} fashion \cite{tufte1990envisioning}.
Small multiples have been used  to visualize annual drought in the U.S. \cite{nytimesSm}, monthly drought in the state of California \cite{latimesSm} or approved harvests per year \cite{harvest}. 
This approach is exemplified in \Cref{fig:mult_a}.

Presenting multiple spatial graphs at the same time allows for identifying the global evolution of temporal data and for detecting patterns.
However, it may be difficult to make local comparisons. 
Moreover, only a short amount of graphs can be displayed at the same time, which makes this approach unsuitable for representing large spatial time series.


Coordinated multiple views \cite{Roberts2007,Andrienko2007} have also been used for spatial time series visualizations. 
The majority of methods using this approach, e.g., \cite{Jern2006, MacIejewski2010, Ferreira2013}, present a spatial graph as the main view, as illustrated in  \Cref{fig:mult_b}. Such spatial graph is usually a heat map or a density heat map. The main view is linked with other views, such as time series graphs, parallel coordinates, histograms, among others.

Frameworks using coordinated multiple views can benefit from the properties of each linked view while avoiding clutter.
Nevertheless, having more views reduces the computational performance and usually implies a higher cognitive load for users.

%
%\cite{MacIejewski2010} used heat maps in a linked view 
%
%\cite{Ferreira2013} use a linked view strategy where they combined a spatial map along with other plots such as time series graphs, scatter plots and histograms. Four strategies were used to visualize spatial data on the map: point clouds, a level-of-detail strategy to reduce the amount of points, a density heat map and a heat map grouped for region.



\section{Visual analytics of spatiotemporal data}
%% Statistycal analysis

%\citeonline{Kamarianakis2003} STARIMA and BVAR
%
%\citeonline{Kyriakidis2001a,Kyriakidis2001}


%% Visual analytics

\citeonline{Demsar2008} propose to use geographically weighted regression to identify spatiotemporal patterns in spatial time series. 
They build a dictionary consisting of different spatiotemporal configurations and then represent each element on the spatial time series using this dictionary. That is, each element is associated to a series of coefficients encoding specific spatiotemporal  behaviors. 
Such coefficients are later grouped using a \emph{Self Organizing Map} (SOM) \cite{Kohonen1995}, which also serves as a mean of visually exploring the results. 
The SOM view is linked with a spatial view and a time series graph to allow the exploration of the data.
Nevertheless, the spatiotemporal patterns dectected are limited by the configuration of the dictionary.


SOMs have also been used  by \citeonline{Andrienko2010} to cluster spatial configurations at different time slices (spatial-in-time), and temporal profiles at different locations (time-in-space). 
When displaying the resulting SOM matrix, similar cells have similar color.
That view is linked with other views allowing the discovery of periodic events, anomalous situations and other patterns.
However, since temporal or spatial information is treated globally, discovering local variations, temporal or spatial, is impossible. 

\citeonline{Kothur2013} group time slices using a hierarchical clustering approach. 
The resulting hierarchy is visualized and used to explore the data, along with other linked views.
The resulting groups only account for global affinity since the similarity measure used was the sum of squared errors between time slices. 


%events \citeonline{Andrienko2010b}
%visual analytics framework 2012}

%\citeonline{MacIejewski2010}


%\citeonline{Shi2011} SAVE sensor network anomaly visulization both visualization and anomaly detection analytics

A popular domain for time-varying information defined on graphs is in the study of urban data. 
%The exploration of such data shares many of the challenges of general spatio-temporal data visualization. 
\citeonline{Ferreira2013} use the start and end information of taxi trips to explore urban mobility and patterns, introducing an efficient storage manager 
to deal with large volume of data. The method is capable of handling different queries such as trips that begin on 
downtown Manhattan and end at the airports. 
However, the exploration of changes in the information is not fully developed, providing only a plot of total events and 
the juxtaposition of geographic renderings for different time slices. 
\citeonline{Doraiswamy2014} consider the same data as one of the case studies, proposing 
a method to support event-guided exploration of large urban data. 
The problem is approached through topological tools and is able to detect events of different scales and shapes. 
Detected events can be used as a parameter for querying, however, the method 
does not provide details on the temporal evolution of the information not detected as an event.

\subsection{Mobility patterns}
\citeonline{Wang2013} consider taxi tracking information that is filtered and matched to road networks. 
This information is used to calculate trajectories and create propagation graphs of the traffic information. 
% Velocity in each road can be viewed as a timeline-based layout or using a geographical map. 
The propagation graphs are used to show concise information about detected events, specifically how the traffic jams propagate. 
In contrast to our approach, their pixel-based layout does not provide an efficient way to observe patterns of change in the information.
\citeonline{Zeng2013} study the visualization of interchange patterns between defined locations, 
introducing the concept of \emph{interchange circos diagrams}. 
The method uses juxtaposition of diagrams to explore temporal changes. However, since the method is based 
on multi-scale information aggregation, users have to interactively explore the information throughout different scales, what can  
hinder the detection of small anomalies. Such remarks can also be made for the work proposed by \citeonline{Andrienko2008}, 
where data aggregation methods are used to explore trajectory-oriented and traffic-oriented views of movement data, 
going beyond the usual spatiotemporal aggregation schemes by considering direction and route information. 
Since they overlay the pixel-based traffic information into the map, only the coarse geographical information is kept during visualization.

\citeonline{Pu2014} present a system for visualizing mobility patterns based on phone call information. 
The visualization is based on a Voronoi diagram computed from cellphone tower location, 
allowing the analysis of people migration between different towers. 
The migration flow is depicted on the edges of the diagram. A similar approach was used by \citeonline{Sun2014} to 
analyze temporal information using roads of a map as a graph. The time-varying information is associated with the edges of the graph
rather than the nodes. The main advantage of Pu and Sun methods is the ability to correlate spatial and
temporal information. Such methods are not suitable, though, for large graphs. Handling levels of detail
is not also a straightforward task. 

A quite different approach is proposed by~\citeonline{chu2014visualizing}, where geographic coordinates are transformed into street names, 
therefore the trajectory of a particular taxi can be expressed as a document and the dataset itself as a document corpora, 
allowing to process the data through nature language processing tools such as~\emph{Latent Dirichlet Allocation}. 
Data variations are expressed through changes in the ``taxi topics'' and depicted using a set of alternative plots. 
However, the visualization is focused on the taxi topic information, thus raw information such as the concentration of taxis in specific position 
and time can not be directly visualized.% I couldn't find any more holes in it. Its very similar to our approach...

\citeonline{Guo2011} introduce a visualization system called TripVista, aiming the exploration of microscopic traffic 
patterns and abnormal behaviors, such as traffic on a particular street junction. The method involves, along with other visualization techniques, 
a timeline visualization derived from Theme-River~\citeonline{Havre2000}, but enriched with directional information.
While this approach is capable of identifying underlying patterns, outliers, and abrupt changes in the data, it is not directly scalable to macroscopic patterns
and can not discriminate changes according to the frequency of the signal.


