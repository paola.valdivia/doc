%!TEX root=../thesis.tex

% Increasing amounts of data are being gathered and made public.    
% Extracting valuable information from these data is usually a difficult task without computer aid.
% Indeed, even complex algorithms cannot perform well in some tasks, such as context-aware event detection, which are more suitable to humans.
% \emph{Visual analytics} stands as a cooperative data analysis paradigm that aims to find effective ways of combining analytical power from humans and computers  \cite{Keim2008}.

Signal processing has long been a fundamental tool in fields 
such as image processing~\cite{oppenheim1999discrete},
computer vision~\cite{granlund:book:2013}, and computer graphics~\cite{zhang2010spectral},  
leveraging the development of filtering mechanisms 
designed to tackle problems such as denoising~\cite{buades2005non,lu2016robust},
anomaly detection~\cite{kang2017hyperspectral}, detail enhancement~\cite{gastal2012adaptive,vallet2008spectral}, 
object registration~\cite{reddy1996fft,litany2017fully}, among others.

Recently, the theory of graph signal processing has emerged as a main alternative
to analyze signals defined on the vertices of graphs~\cite{Shuman2013}.
It focuses on adapting tools and operators, such as Fourier and Wavelet
transforms, to the context of graphs, opening a multitude of possibilities
to the exploration and understanding of static and time-varying phenomena
in irregular domains.

Similarly to the case of regular domains, most of the analysis derived
from Graph Fourier (GFT) and Graph Wavelet Transforms 
(GWT)~\cite{Mohan2014,Valdivia2015,Tremblay2014,dal:tvcg:2017}, relies on the
processing of coefficients associated with certain basis functions
defined on the vertices of a graph~\cite{Hammond2011}. However,
the irregular nature of graph domains makes the coefficients sensitive
not only to the properties of the signal under analysis, but also to
the topology of the graph \cite{Shuman2015}.  


The sensitivity of
the GFT and GWT to signal variation across irregular domains is an
evident and noteworthy characteristic of these transforms, which can
be used to understand phenomena related to daily life. For example,
the street map of a city can be represented as a graph and the way
the data under analysis varies across streets, from corner to corner,
may reveal important aspects of city's dynamics. 

Another interesting scenario is the case of unstructured data, which we take to be high-dimensional points
without intrinsic connectivity. 
It is well known that data acquisition can lead to noisy information, thus spectral graph filtering schemes can be
useful to reduce noise and
emphasize structures hidden in the data. 
Regardless of the possibilities, not many efforts
have been devoted to incorporating
graph signal processing tools into visual analysis for data exploration.


In this dissertation, we address the gap mentioned above 
to support effective visual data analysis that relies on graph signal processing tools.
Specifically, we address two problems, firstly we show how to assist high-dimensional 
data visualization using signal processing tools. The second addressed problem  is how 
to leverage GWT to enable the visual analysis of urban data directing users to interesting patterns and changes.
We also discuss some aspects on how to understand the interplay between GFT and GWT coefficients 
and how to interpret this coefficients so they can be used to uncover patterns and 
interesting phenomena hidden in the data.


%\section{Addressed problem}
%%Visual analytics of time-varying data is a challenging task and has been addressed by many customized methods. 
%%\cite{Aigner2007} present3s a systematic categorization of visual concepts for analyzing time-varying data.
%%Such categorization is based in three criteria: time, data and representation; each of one including respective sub-criteria.
%%The addressed problem will be exposed according to such categorization.
%%
%%\emph{Temporal dimension:} time points or time intervals can be used as temporal primitives. In our work we 
%%
%
%Within the \emph{Visual Analytics Science}, analysis of spatial time series is a challenging task.
%\cite{Aigner2007} state that time-varying data visualizations should easily reflect three main issues regarding: the characteristics of the time dimension, nature of the data and how data is being represented.
%The addressed problem will be exposed according to the categorization presented by \cite{Aigner2007}:
%
%\begin{itemize}
%\item The kind of data considered on the analysis will be univariate spatial data, such as urban data. The spatial dimensions will be represented by a spatial graph.
%\item The time dimension will be treated as linear time intervals and will be represented on the nodes of each spatial graph.
%\item 
%\end{itemize}



% \section{Thesis Statement}



In summary, this dissertation gathers techniques and results developed and published during the doctorate term. Specifically, it puts together the results from the following works:

\begin{enumerate}
\item ``Wavelet-based visual analysis for data exploration'' Col, A. D., \textbf{Valdivia, P.}, Petronetto, F., Dias, F., Silva, C. T., and Nonato, L. G. (2017a). \textit{Computing in Scince Engineering}, 19(5):85–91  \cite{dal2017wavelet}.
\begin{itemize}
  \item a discussion on some aspects of the GFT and GWT, providing examples of how the signal and the topology of the graph impact the computation of coefficients; 
  \item a set of controlled experiments, using real and synthetic data, to illustrate how the topology and signal can affect the result of GFT and GWT.
\end{itemize} 
% \item ``Massaging Visualizations with Graph Spectral Filtering'' \textbf{Valdivia, P.}, Falc\~ao, A., Silva, C., and Nonato, L. G. (to be submitted).\\ The contributions of this work are:
% \begin{itemize}
%   \item the first study of using filtering techniques as an integral part of high-dimensional data visualization; 
%   \item a novel methodology to filter high-dimensional data aimed at improving visualizations;
%   \item a quantitative study measuring the benefits of the proposed methodology for visualization purposes.
% \end{itemize} 
\item ``Wavelet-based visualization of time-varying data on graphs'' \textbf{Valdivia, P.}, Dias, F.,  Petronetto, F., Silva, C. T., and Nonato, L. G. (2015).  \textit{IEEE Transactions on Visualization and Computer Graphics}, PP(99):1–1 \cite{Valdivia2015}. \\
The contributions of this work are:
\begin{itemize}
  \item a novel method for the visual analysis of time-varying data
  defined on the nodes of a graph which combines graph wavelets,
  pattern recognition/classification, and stacked graph
  metaphor.
  \item a methodology to classify graph nodes based on their wavelet
  coefficients that enables spatial and temporal visual analysis of
  data variation.
  \item results using synthetic data and a real case study that
  showcase the capability and potential of the proposed visual
  analytic tool in revealing interesting phenomena and events from
  massive time-varying data, including Citi Bike rents (in the
  supplementary material) and taxi trips in downtown Manhattan.  
\end{itemize} 
\end{enumerate}


Additional works developed in the doctorate period:

\begin{enumerate}
\item ``Wavelet-based visual analysis of dynamic networks''. Col, A. D., \textbf{Valdivia, P.}, Petronetto, F., Dias, F., Silva, C. T., and Nonato, L. G. (2017b). \textit{IEEE Transactions on Visualization and Computer Graphics}, PP(99):1–1 \cite{dal:tvcg:2017}.
\item ``Watersheds on Hypergraphs for Data Clustering''. Dias, F., Mansour, M. R., \mbox{\textbf{Valdivia, P.}}, Cousty, J., and Najman, L. (2017), \textit{International Symposium on Mathematical Morphology and Its Applications to Signal and Image Processing.} PP 211–221 \cite{dias2017}. 
\item ``Analyzing Dynamic Hypergraphs with Parallel Aggregated Ordered Hypergraph Visualization''. \textbf{Valdivia, P.}, Buono, P., Plaisant C., and Fekete, J.-D. (2018).\\
Submitted to IEEE InfoVis 2018. (IEEE Information Visualization (InfoVis) Conference).
\end{enumerate}


\section{Outline}

This dissertation is organized in five main parts: The first part  (\Cref{chapter:gsp}), which serves as background for the 
methodologies developped in the second part,
covers specific concepts on graph signal processing and spectral graph
filtering. The second part (\Cref{chapter:gsp_f}), which is part of the contributions presented in this dissertation,  presents in detail the proposed methodology
to apply graph spectral filtering for enhancing the quality of high-dimensional data
visualizations. The third part (\Cref{chapter:gwt}) presents concepts on the Graph
Wavelet Transform, concepts that support our methodolology presented in the fourth part; 
it also presents a discussion on how the graph topology and
properties of the signal  affect the wavelet coefficients. 
The fourth part (\Cref{chapter:spatiotemporal,chapter:waviz}) shows a methodology, developed during the doctorate, on how 
Wavelet coefficients can enable visual analysis in urban data to identify
patterns and outliers. Finally, the fifth part (\Cref{chapter:conclusions}) present the
conclusions and future work.


%
