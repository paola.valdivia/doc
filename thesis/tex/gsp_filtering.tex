%!TEX root=../thesis.tex

We propose a methodology to adapt the theory of Graph Signal Processing (GSP) to filter
high-dimensional data towards enhancing the quality of visualizations.
The proposed filtering mechanisms result in less cluttered visualizations while highlighting important structures contained in
the data, making the visual layouts cleaner and more informative, as illustrated in \Cref{fig:teaser}.
The effectiveness of the proposed methodology is shown through a set of quantitative measures
that gauge the impact of the filtering process to visualizations.  
The performed experiments reveal that even simple band-pass filters can remarkably improve visualizations
such as parallel coordinates, scatter plots and multidimensional projections, attesting the importance
of incorporating filtering mechanisms as integral part of high-dimensional data visualization process.

\begin{figure}
\centering
\includegraphics[width=\linewidth]{figs/gsp_filtering/teaser.png}
  \centering
   \caption[Graph spectral filtering towards enhancing the quality of visualizations.]{Graph spectral filtering can reduce noise and emphasize structures present in the data. From left to right, 
       multidimensional projection, scatter plot matrix, and parallel coordinates visualizations before and after data filtering.}
 \label{fig:teaser}
\end{figure}

%The relevance of our methodology is also shown in a practical application involving visualization assisted image labeling,
%reducing users' effort to label images from clusters present in the visual space.

%\begin{classification} % according to http://www.acm.org/class/1998/
%\CCScat{Computer Graphics}{I.3.3}{Picture/Image Generation}{Line and curve generation}
%\end{classification}

\section{Signal processing in visualization}


In this section we provide an overview of techniques that rely on signal processing apparatus 
as underlying resource to support visualization. Indirect filtering such as aggregation~\cite{Andrienko2008},
summarization~\cite{lins2013nanocubes}, and data pruning/selection~\cite{wegman2003visual} are not in our scope and
will not be considered. The goal in this section is not to provide a comprehensive survey about methods that somehow
make use of signal processing concepts to leverage visualizations. 
Rather, we focus on the diversity of use of signal processing tools in the context of visualization,
emphasizing the broad scope of the classical theory and the potential of its recent graph-based variant.  

\paragraph*{Classical Signal Processing in Visualization} The classical signal processing machinery has been employed to support a variety of visualization methods. 
In the context of scientific visualization, for example, volume rendering techniques have built upon Fourier transform to properly 
sample volume rendering integrals~\cite{bergner2006spectral,schulze2003integrating} and to 
filter aliasing effects introduced during the perturbation of opacity maps~\cite{khlebnikov2013noise}. 
In fact, the need for data filtering resource has long been acknowledged
in context of volume rendering~\cite{sakas1995preprocessing,viola2003hardware}, being investigated until the present time~\cite{solteszova2017output}.
Fourier transform is also the basis of methods designed to identify and visualize patterns in vector fields~\cite{ebling2005clifford,reich2015decomposition}.
Filters designed to operate in spatial domains has also been employed to remove low-frequency components from  
spot-noise based vector field visualizations~\cite{de1995enhanced}.

Classical signal processing also plays a role in information visualization. A good example is the edge bundling technique
proposed by~\citeonline{lhuillier2017ffteb}, which maps the bundling problem to the spectral domain in order to 
speed up computation through the Fourier convolution property.
Image-based filtering has been applied to detect outliers and trends 
in 2D binned data associated to pairs of adjacent axes in parallel coordinates visualizations,
enabling output-oriented visualizations~\cite{novotny2006outlier}.
Low-pass filtering has been applied to smooth out density maps
in illustrative parallel coordinates~\cite{mcdonnell2008illustrative}.

\paragraph*{Graph Signal Processing in Visualization}
The more recent framework of graph signal processing has fostered some developments in visualization.
For instance, the method proposed by~\citeonline{Valdivia2015}, discussed later in this dissertation on \Cref{chapter:waviz}, relies on 
graph wavelet transform to identify low and high frequency patterns in graphs where time varying
data is associated to the nodes.
A similar approach has been proposed by~\citeonline{dal:tvcg:2017} in the context of dynamic networks, 
building upon graph wavelet coefficients to visually identify time slices where the network 
assumes ``low'' and ``high-frequency'' configurations.
Graph Fourier transform is also the basis of the method proposed by~\citeonline{huang2016graph},
which decomposes brain signals into pieces that correspond to smooth or rapid signal variations, enabling
the visual analysis of different brain activities. 

From the discussion above, it is clear the importance of signal processing in the context of visualization.
However, the use of filtering mechanism as a resource to reduce noise and emphasize important structures 
present in the data has been little explored, mainly in information visualization applications. 
The present dissertation comes to shed light on this issue, proposing a methodology 
that enables the use of graph filtering mechanisms to leverage more informative visualizations.

%------------------------------------------------------------------------
\section{Graph Spectral Filtering in Multidimensional Data}
\label{sec:method}


The GSP theory described in \Cref{chapter:gsp} is designed to operate in scalar signals defined on
the vertices of a graph. Our goal, though, is to employ spectral filtering to
sift high-dimensional data, cleaning noise and emphasize structures towards producing better visualizations. 

The definition of a graph structure is the first step to adapt GSP to operate in high-dimensional data. 
%From the graph one can computed the graph Laplacian and its spectral decomposition, which comprises the
%basis of the graph filtering mechanism.
We adopt the simple approach of defining the graph $G$ as the $k$-nearest graph (KNN-graph) of the data.
As discussed in \Cref{sec:discus}, more sophisticated alternatives can be employed to build a graph from high-dimensional data,
however, our methodology already presents quite satisfactory results with the KNN-graph.

Let $\mathcal{T}=\{\tau_1,\tau_2,\ldots,\tau_n\}$ be a set of points in an $m$-dimensional space, 
$\tau_i\in\mathbb{R}^m,\, i=1,\ldots,n$ and $G$ be the KNN-graph derived from $\mathcal{T}$. 
From now on we will not make any distinction between a data instance $\tau_i$ and its corresponding
node in the KNN-graph, using the same symbol for both.  

We denote by $\tau^j$ the $n$-dimensional array containing the $j$-th attribute (coordinate) 
of all instances in $\mathcal{T}$. The array $\tau^j$ can be seen as a scalar function (signal) $\tau^j: V\rightarrow\mathbb{R}$ that assigns 
to each node $\tau_i$ the value of its $j$-th attribute $\tau^j_i$.
Since $\tau^j$ is a signal defined on the nodes of $G$, we can apply graph spectral filtering
to process $\tau^j$. 
In other words, given a spectral filter $\hat{h}$, we can compute the filtered version $\tilde{\tau}^j$ of the $j$-th 
attribute of the data by replacing $f$ to $\tau^j$ in \Cref{eq:gf}.
Repeating the process to all attributes, we can filter the whole data set.
In summary, our approach is made up of three main steps: 1) build the KNN-graph from $\mathcal{T}$,
2) convert each attribute to a signal defined on the nodes of the KNN-graph, 3) filter the signals (attributes) based on a given spectral filter $\hat{h}$.



\Cref{fig:filter_action} illustrates the proposed methodology and provides some intuition about the behavior 
of the filtering process when applied to data attributes.
The point cloud depicted in \Cref{fig:filter_action_tl} was generated by drawn $40$ points from three bivariate Gaussians centered
at the points $(0,1),\, (1,1),\, (2,1)$, all with covariance matrix given by $\sigma I$, where $I$ is the $2\times 2$ identity
matrix and $\sigma=0.1$. 
\Cref{fig:filter_action_tr} shows the $k$-nearest neighbor graph of the points with $k=5$.
\Cref{fig:filter_action_ml,fig:filter_action_mr} show the distribution of points after applying a low-pass filter
to the signals corresponding to the $x$ and $y$ coordinates, respectively.
The low-pass filter smooths out the signals, making the points more tightly grouped.
Moreover, the filter operates based on the graph topology, smoothing more stringently the coordinates of the points that are more densely connected.
This fact can be noticed in \Cref{fig:filter_action_ml}, where the low-pass
filter squished horizontally each of the three groups that are more densely connected, making them better delineated.
A similar behaviour is observed in \Cref{fig:filter_action_mr}, where the $y$ coordinate is smoothed out.
\Cref{fig:filter_action_bl} shows the configuration of the points after filtering both the $\tilde{x}$ 
and $\tilde{y}$ coordinates. The presence of three distinct groups of points is clearly discernible in 
\Cref{fig:filter_action_bl}, which is not the case in original layout (\Cref{fig:filter_action_tl}). 

Another interesting outcome of the filtering scheme is the better understanding of how each attribute is distributed. 
\Cref{fig:filter_action_br} depicts histograms of the $x$ (left column) and $y$ (right column) coordinates
before (top row) and after (bottom row) the graph-based low-pass filtering.
Comparing the histograms of the $x$ coordinate before (top left) and after (bottom left)
the filtering process one can clearly see that the $x$ coordinates
become mostly concentrated in three specific values after filtering (bottom left), indicating the presence of three groups of points.
The three groups are not so discernible in the original data histogram (top left).



\begin{figure}
\centering
\subfig{}{figs/gsp_filtering/3gauss-scatter.pdf}{\label{fig:filter_action_tl}}{4.5cm}
\subfig{}{figs/gsp_filtering/3gauss-graph.pdf}{\label{fig:filter_action_tr}}{4.5cm}
\subfig{}{figs/gsp_filtering/3gauss-fx.pdf}{\label{fig:filter_action_ml}}{4.5cm}
\subfig{}{figs/gsp_filtering/3gauss-fy.pdf}{\label{fig:filter_action_mr}}{4.5cm}
\subfig{}{figs/gsp_filtering/3gauss-fscatter.pdf}{\label{fig:filter_action_bl}}{4.5cm}
\subfig{}{figs/gsp_filtering/3gauss-hist.pdf}{\label{fig:filter_action_br}}{4.5cm}
\caption[Low-pass filter
to the signals corresponding to the $x$ and $y$ coordinates of three bivariate Gaussians.]{
a) Points drawn from three bivariate Gaussians centered at $(0,1),\, (1,1),\, (2,1)$ and constant diagonal covariance matrix;
b): $k$-nearest neighbor graph with $k=5$; 
c) and d): smoothing out the $x$ and $y$ coordinates respectively by a graph spectral low-pass filtering;
e): configuration of points after filtering both the $x$ and $y$ coordinates; 
f): distribution of the $x$ and $y$ coordinates (left and right columns respectively) before and after the filtering process (top and bottom rows respectively).
}
\label{fig:filter_action}
\end{figure}

%Although simple, the example presented in Figure~\eqref{fig:filter_action} helps to build intuition on how the proposed filtering
%schemes operates. 

It is clear from the construction above that the proposed methodology handles each attribute as an independent signal. 
Since each attribute is seen as a signal, why not to resort the classical signal processing theory 
rather than apply to the more intricate and costly framework proposed above?
The answer for this question is simple: to handle an attribute as a ``conventional'' signal we have to set a domain
for it to live in. There is no natural way to define a domain for an attribute. Suppose for example we
want to handle an attribute $\tau^j$ as an one-dimensional signal. 
In this case we have to define the pairs $(x_i,\tau^j_i)$ in order to process $\tau^j$ as a function.
However, fixing a sequence $\{x_1,\ldots,x_n\}$ means to impose an order to $\tau^j_i$ that in fact doesn't exist. 

The top image in \Cref{fig:inset} depicts the $x$ coordinates of the points in \Cref{fig:filter_action_tl} as 
an one-dimensional signal were the domain is given by the index of the corresponding points.
If we chose another order for the nodes, the shape of the signal changes completly, as illustrated at
the bottom image in \Cref{fig:inset}. Therefore, unless one can come up with a good strategy to define a domain for the attribute signals,
it doesn't make sense to filter them.
In contrast, the KNN-graph used in the proposed formulation comprises a natural domain where the attribute signals
can be defined.

\begin{figure}
\centering
    \shadowimage[width=.3\textwidth]{figs/gsp_filtering/xassignals.png}
    \caption{Graph attributes represented as a one-dimensional signal.}\label{fig:inset}
\end{figure}

In the following we analyze the impact of the proposed filtering technique in different visualization scenarios. 
%showing the gain in building visualization from filtered data.

\section{Results}
\label{sec:results}

In this section we describe a set of experiments that shows the usefulness of the proposed filtering scheme 
to improve the quality of visualizations. More specifically, we apply our methodology to high-dimensional data
visualization in three different scenarios: scatter plot, parallel coordinates and multidimensional projection. 
In order to assess the impact of the graph filtering scheme in the visualizations we employ quantitative measures 
designed to assess the quality of scatter plot, parallel coordinates and multidimensional projection layouts. 
Before describing and discussing the results, we present the datasets used in our experiments.

\subsection{Data Sets}


Four different data sets are used in the experiments, a synthetic dataset (Blobs), the Ecoli and Wine data sets from the UCI 
Repository~\cite{Lichman:2013}, and a proprietary data set (Eggs) containing instances with 5000 attributes which correspond 
to the output layer of an Encoder-Decoder Neural Network~\cite{VincentJMLR2010} applied to images of helminth eggs. 
All datasets have labeled instances and their number of instances, classes, and dimensionality are detailed in \Cref{table:datasets}.
The synthetic dataset was generated using the scikit-learn~\cite{scikit-learn} method \texttt{make\_blobs} 
with three classes and standard deviation equal to $2.3$.
Only scalar attributes from the dataset Ecoli are used (categorical attributes are desregarded).
%To show how our methodology can reveal the underlying structure of the data, we explore three well known visualization techniques.
%Specifically, we show how scatter plots, parallel coordinates, and multidimensional projections are improved qualitatively and quantitatively by using the described methodology.
\begin{table}
\centering
\caption{Data sets used in the experiments.}
\label{table:datasets}
\begin{tabular}{ccccc}
\toprule
Name & Size & Dim & \# of classes & Source\\
\midrule
Blobs & 200 & 5 & 3 & synthetic \\
Ecoli & 336 & 5 & 8 & \cite{Lichman:2013} \\
Wine & 178 & 13 & 3 & \cite{Lichman:2013} \\
Eggs & 320 & 5000 & 8 & proprietary \\
\bottomrule
\end{tabular}
\end{table}


\subsection{Scatter Plot Matrix}
\label{seq:splom}

\begin{figure}
  \centering
  \shadowimage[width=\linewidth]{figs/gsp_filtering/blobs_splom_original.png}\\[.3cm]
  \caption{Scatter Plot Matrix of the synthetic dataset: original data. 
  \label{fig:blobs_splom_original}}
\end{figure}

\begin{figure}
  \centering
  \shadowimage[width=\linewidth]{figs/gsp_filtering/blobs_splom_filter_low.png}\\[.3cm]
  \caption{Scatter Plot Matrix of the synthetic dataset: low-pass filtered data.
  \label{fig:blobs_splom_low}}
\end{figure}

\begin{figure}
  \centering
  \shadowimage[width=\linewidth]{figs/gsp_filtering/blobs_splom_filter_enh.png}\\[.3cm]
  \caption{Scatter Plot Matrix of the synthetic dataset: enhancement filtered data.
  \label{fig:blobs_splom_enh}}
\end{figure}

\Cref{fig:blobs_splom_original} shows the scatter plot matrix generated from the synthetic dataset.
\Cref{fig:blobs_splom_low,fig:blobs_splom_enh} are the scatter plot matrices of filtered versions of
the synthetic dataset with a 
low-pass and an enhancement graph filter, respectively, applied to the data. 
%This dataset is composed of three clusters modeled as Gaussian blobs in $\mathbb{R}^5$. 
%Clusters are depicted in different colors. 

Consider the upper left $2\times 2$ sub-matrices corresponding to the first two attributes. Analyzing the histograms 
of the filtered sub-matrices (\Cref{fig:blobs_splom_low,fig:blobs_splom_enh}) one can easily see that the distribution of the first and second attributes
are concentrated in three intervals, indicating the existence of three well defined groups. The identification
of the three groups is not so evident in the histograms of the original data (top image). Moreover, the correlation between the two
attributes is more clearly revealed in the filtered scatter plot and based on the histograms users could infer the presence
of three groups in the correlated attributes. 
Regarding the lower right $3\times 3$ sub-matrix formed by the three last attributes, the histograms do not indicate those attributes
are segregating the data into groups. However, the three groups are well separated when filtering is used to generate the scatter plots, mainly in 
scatter plots corresponding to the enhancement filter. The three groups are noway obvious in the scatter plots of raw data.


%This dataset shows how applying a spectral filter reveals how the structure of the dataset influences each pair of attributes. 
%Particularly, since clusters in this dataset are well defined, the distribution of clusters in each pair of attributes is uncovered. 
%Low-pass filtering the data shows mainly the directions of clusters within pairs of attributes, and applying an enhancement filter also shows how data spreads across this directions.
%Additionally, notice how correlation between attributes 0 and 1 is emphasized by low-pass filtering the data (middle of Figure~\ref{fig:blobs_splom}) while applying an enhancement filter (bottom of Figure~\ref{fig:blobs_splom}) reveals the shape of the data while also emphasizing correlation between the attributes. 
%cluster separation, correlation, trends




%\paragraph*{Quality Metrics}
\paragraph*{Quantitative evaluation}
In order to quantitatively attest the better quality of the visualizations generated from the filtered data we compare the layouts using four
different metrics widely employed to gauge the quality of scatter plots, namely, silhouette, homogeneity, completeness and adjusted rand index.
%The better quality of the filtered dataset from Figure~\ref{fig:blobs_splom} is attested with the quality metrics shown in Table~\ref{table:metrics_sp_blobs}. 
%These metrics are intended to see the behavior of the underlying cluster structure of raw versus filtered data.
The silhouette metric~\cite{rousseeuw1987silhouettes} accounts for both the cohesion and separation of grouped instances.
Homogeneity, completeness~\cite{rosenberg2007v} and adjusted rand index~\cite{hubert1985comparing} compare the ground truth labels against the ones obtained by clustering the projected points in the 2D layout. 
More specifically, {completeness} gauges if members of a given class are contained in only one cluster. 
{Homegeneity} measures if all clusters contain only data points which are members of a single class. 
{Adjusted rand index} (adjusted\_rand) measures the similarity between the ground truth labels and the clusterized groups.
The larger the value of the metrics, the better the quality of the layout. 
The affinity propagation method~\cite{frey2007clustering} was used as clustering method as it does not require the number of clusters as input parameter. 
% From the values, we can see that applying a low-pass or enhancement filtered data reveal 

% \begin{table}[!h]
% \caption{Quality metrics for the synthetic dataset shown in Figure~\eqref{fig:blobs_splom}.}
% \centering
% \begin{tabular}{cccc}
% \toprule
% Metric & Raw & Low-pass filtering & Enhancement\\
% \midrule
% silhouette            & 0.299  & \textbf{0.679} &  0.622 \\
% homogeneity           & 0.916  & 0.910 &  \textbf{0.949} \\
% completeness          & 0.390  & 0.507 &  \textbf{0.564} \\
% % v-measure             & 0.547  & 0.651 &  0.707 \\
% % adjusted mutual info  & 0.373  & 0.497 &  0.556 \\ 
% adjusted rand index    & 0.258  & 0.486 &  \textbf{0.559} \\
% \bottomrule
% \end{tabular}
% \label{table:metrics_sp_blobs}
% \end{table}


\begin{figure}
\centering
\shadowimage[width=.65\linewidth]{figs/gsp_filtering/splom_proj_metrics.pdf}
\\[.3cm]
%\includegraphics[width=.98\linewidth]{figs/gsp_filtering/blobs_splom_metrics.pdf}
\caption[Average quality measure of the synthetic dataset scatter plots.
The larger the averages the better.]{Average quality measure of the synthetic dataset scatter plots depicted in \Cref{fig:blobs_splom_original,fig:blobs_splom_low,fig:blobs_splom_enh}.
The larger the averages the better.}\label{fig:metrics_sp_blobs}
\end{figure}

The result of applying the metrics described above to the scatter plots depicted in \Cref{fig:blobs_splom_original,fig:blobs_splom_low,fig:blobs_splom_enh} 
is presented in \Cref{fig:metrics_sp_blobs}. 
The values correspond to the average score of each metric over all layout of the scatter plots matrix. 
Notice that scatter plots built from filtered data are better for all the metrics and for two of them (completeness and adjusted\_rand) 
the improvement is considerable. 

\subsubsection{Parallel Coordinates}


\begin{figure}
\centering
\subfigw{Original dataset}{figs/gsp_filtering/wine_pc_overplotting_order.pdf}{\label{fig:blobs_pc_original}}{\linewidth}\\
\subfigw{Low-pass filtered data}{figs/gsp_filtering/wine_filtered_pc_overplotting_order.pdf}{\label{fig:blobs_pc_original}}{\linewidth}
\caption{Parallel Coordinates view of the wine dataset with over-plotting optimal axes arrangement.}\label{fig:wine_pc_overploting}
\end{figure}

\begin{figure}
\centering
\subfigw{Original dataset}{figs/gsp_filtering/wine_par.pdf}{\label{fig:blobs_pc_original}}{\linewidth}\\
\subfigw{Enhancement filtered data}{figs/gsp_filtering/wine_enhance_par.pdf}{\label{fig:blobs_pc_original}}{\linewidth}
\caption{Parallel Coordinates view of the wine dataset with parallelism optimal axes arrangement.}\label{fig:wine_pc_parellelism}
\end{figure}


\begin{figure}
\centering
\shadowimage[width=0.5\linewidth]{figs/gsp_filtering/pc_metrics.pdf}
\\[.3cm]
\caption[Parallel coordinates quality measures for the original (green), low-pass filtered (purple), and enhancement filtered (orange) data.]{Parallel coordinates quality measures for the original (green), low-pass filtered (purple), and enhancement filtered (orange) data.
The smaller the values of l\_crossing, median angle, and over-plotting the better. The larger the p\_norm the better.
Each dot corresponds to the quality between a pair of axes in the layout.}\label{fig:wine_pc_metrics}
\end{figure}


The second set experiments analyze the impact of graph filtering in parallel coordinates visualization.
The top images in \Cref{fig:wine_pc_overploting,fig:wine_pc_parellelism} 
show parallel coordinates visualizations of the wine dataset using \emph{over-plotting} and \emph{parallelism} metrics 
(see details below) to find the optimal order of the axes. 
The bottom images in \Cref{fig:wine_pc_overploting} and \Cref{fig:wine_pc_parellelism} are parallel coordinates 
visualizations of low-pass and enhancement filtered versions of the data also using 
{over-plotting} and {parallelism} metrics, respectively, optimize the order of the axes.
%The same optimal axes arrangement schemes employed to visualize the raw data were applied to 
%generate the Parallel Coordinates visualization of filtered data.  
It is clear the visual improvement of the layouts when filtered data is used to build the visualizations. 
In this particular application, low-pass filter leads to pleasant layouts with are very well defined groups. 

%\begin{figure}[!thb]
%\centering
%\includegraphics[width=0.9\linewidth]{figs/gsp_filtering/wine_par.pdf}\\
%\includegraphics[width=0.9\linewidth]{figs/gsp_filtering/wine_enhance_par.pdf}
%\caption{Parallel Coordinates view of the wine dataset with maximum parallelism. Top: raw dataset. Bottom: enhancement filter applied to the dataset.}\label{fig:wine_pc_par}
%\end{figure}

%\paragraph*{Quality Metrics}
\paragraph*{Quantitative evaluation}

We rely on the methodology proposed by~\citeonline{dasgupta2010pargnostics} 
to measure the quality of parallel coordinates layouts. In short words, the reasoning is to define
a metric to evaluate the quality between every pair of axes in the layout and apply a branch-and-bound optimization 
to rearrange the axes so as to optimize the metric. 
Once an optimal arrangement is reached, the metric can once again be applied to
quantify the quality of the layout. 
In our experiments we assess the layout with four distinct quality metrics,
line crossings, over-plotting, median angle, and parallelism.
Line crossings (l\_crossing) measures the number of intersections between line segments in the layout,
median angle accounts for the median crossing angle of the line segments,
over-plotting indicates the information loss due to limited number of screen pixels, and
parallelism (p\_norm) measures how parallel the line segments are.
Except for the parallelism metric, smaller values are better.
More details about the mathematical definitions and computational aspects of the metrics 
can be found in the work by~\citeonline{dasgupta2010pargnostics}.

%Since the order of the attributes affects the parallel coordinates view, we use the strategy from~\cite{dasgupta2010pargnostics} to find an optimal order.
%\cite{dasgupta2010pargnostics} defines some metrics in the image space that are computed between every pair of attributes.
%Then, the branch and bound method is used to find the combination that optimize a selected metric.
%These metrics are also used to asses the quality of the visualizations before and after filtering the dataset.
%The selected metrics include the number of line crossings angles, overplotting, median angles, and parallelism.
%The optimum for all metrics, except parallelism, is to minimize the value; for parallelism, the maximum optimizes the plot.

%In Figure~\ref{fig:wine_pc_overplotting} we show the parallel coordinates view with minimized overplotting for raw (top) and filtered data (bottom). 
%For this example, a low-pass filter was applied. 
%In Figure~\ref{fig:wine_pc_par} parallel coordinates with maximum parallelism is shown for the raw (top) and filtered (bottom) wine dataset. Here, an enhancement filter was used.
%It can be seen that after filtering the data the views are less cluttered than their corresponding raw versions.
%Also, the underlying trends in the dataset are revealed with both filters.

\Cref{fig:wine_pc_metrics} depicts the result of applying the metrics to the ``optimum'' parallel coordinates layouts between each pair of axes.
%Values for raw data are depicted using blue, red represents values for low-pass filtered data, and green represent metrics when applying an enhancement filter. 
Notice that for l\_crossing, over-ploting, and median\_angles, the metric's values (each dot is a quality measurement made between a pair of axes)
corresponding to the layouts produced from filtered data are more concentrated in the lower part of the plot while for the parallelism metric
the values are on the higher part, indicating the superior quality of the layouts generated from filtered data.

\begin{figure}
\centering 
\includegraphics[height=6.5cm]{figs/gsp_filtering/ecoli_lamp.png}
\includegraphics[height=6.5cm]{figs/gsp_filtering/ecoli_lamp_enh.png}
\caption{Lamp projection of the Ecoli dataset. Left: original data.  Right: enhanced filtered data.}\label{fig:ecoli_proj}
\end{figure}

\subsubsection{Multidimensional Projections}

\begin{figure}
\centering
\begin{subfigure}{\linewidth}
\centering
\includegraphics[height=6.5cm]{figs/gsp_filtering/eggs_lamp.png}
\includegraphics[height=6.5cm]{figs/gsp_filtering/eggs_lamp_low.png}
\caption{}
\label{fig:eggs_proj_lamp}
\end{subfigure}
\begin{subfigure}{\linewidth}
\centering
\includegraphics[height=6cm]{figs/gsp_filtering/eggs_pca.png}
\includegraphics[height=6cm]{figs/gsp_filtering/eggs_filtered_low_pca.png}
\caption{}
\label{fig:eggs_proj_pca}
\end{subfigure}
\caption{a) Lamp projection of the Eggs dataset using the original (left) and low-pass filtered (right) data.
b) PCA projection of the Eggs dataset using the original (left) and low-pass filtered (right) data.}\label{fig:eggs_proj}
\end{figure}


The last set of experiments assess the impact of the proposed filtering methodology in layouts produced by
multidimensional projections. We employ the LAMP technique~\cite{joia2011local} and PCA~\cite{hotelling:jep:1933} as projection methods.
We choose LAMP and PCA because they are well known projection methods widely used in visualization applications, but any other projection
technique could be chosen.
Two datasets are used in the experiments, the Ecoli and the Eggs datasets (see \Cref{table:datasets}).

\Cref{fig:ecoli_proj} shows the result of projecting the original Ecoli data (left) and its enhancement filtered version (right) using LAMP. 
%Each cluster is represented using a different color. On top of Figure~\ref{fig:ecoli_proj} the LAMP projection of raw dataset is depicted, and in bottom the LAMP projection of the same dataset using an enhancement filter is shown. 
Comparing both projections is easy to see that groups are better defined when filtered data is used as input for the projection.  
In particular, notice the good separability of the pink and gray classes in the filtered projection. Even the blue class that is completely entangled
with the purple class in the original plot becomes better concentrated in the layout with filtered data.


\Cref{fig:eggs_proj} shows the result of visualizing the Eggs data set using LAMP and PCA. 
Eggs is a complex data set with instances embedded in a space with $5000$ dimensions, 
what makes projections based on distance information, as is the case of LAMP, more prone
to distortions (a consequence of the curse of dimensionality). 
Large dimensional data is typically handled via PCA.
Notice how better defined are the groups in both LAMP and PCA layouts when filtered data is used as input for the projections. 
In particular, with exception of the dark blue and green groups, the low-pass filter has untangled all the other groups pretty well,
mainly in the LAMP layout (\Cref{fig:eggs_proj_lamp} left), an impressive result when we compare to the original filtered  data layout.


\paragraph*{Quantitative evaluation}
The quality of the multidimensional projection layouts are assessed using the same metrics employed to
evaluate scatter plot layouts (\Cref{fig:eggs_proj}). The results are summarized 
in \Cref{fig:proj_metrics}. 
Once again, layouts produced from filtered data present better quality for all the metrics.

\begin{figure}
\centering
\subfigw{Ecoli}{figs/gsp_filtering/ecoli_proj_metrics.pdf}{\label{fig:ecoli_proj_metrics}}{.75\linewidth}\\
\subfigw{Eggs}{figs/gsp_filtering/eggs_proj_metrics.pdf}{\label{fig:ecoli_proj_metrics}}{.75\linewidth}
\caption[Quality measure of projection layouts using the original (green), low-pass (purple), and enhancement filtered (orange) data.]{Quality measure of projection layouts using the original (green), low-pass (purple), and enhancement filtered (orange) data. 
a) Metrics applied to LAMP (left) and PCA (right) projections of the Ecoli dataset;  
b) Metrics applied to LAMP (left) and PCA (right) projections of the Eggs dataset. }\label{fig:proj_metrics}
\end{figure}


% \begin{table}[htbp]
% \caption{Quality metrics for Ecoli and Eggs datasets shown in Figures~\eqref{fig:ecoli_proj}~and~\eqref{fig:eggs_proj}.}
% \centering
% \begin{tabular}{cccc}
% \toprule
% \multicolumn{4}{c}{\textbf{Ecoli}}\\
% \midrule
% Metric & Raw & Low-pass filtering & Enhancement\\
% \midrule
% silhouette            & 0.470 &  0.572 & \textbf{0.588} \\
% homogeneity           &  0.571 & \textbf{0.657} &  0.633 \\
% completeness          &  0.324 &  0.431 &  \textbf{0.427}\\
% adjusted rand index    &  0.145 & 0.145 & \textbf{0.292} \\
% \midrule
% \multicolumn{4}{c}{\textbf{Eggs}}\\
% \midrule
% Metric & Raw & Low-pass filtering & Enhancement\\
% \midrule
% silhouette            & 0.523 & \textbf{0.575} & 0.541 \\
% homogeneity           &  0.500 & 0.616 &  \textbf{0.650} \\
% completeness          &  0.357 &  \textbf{0.484} &  0.480\\
% adjusted rand index    &  0.187 & 0.280 & \textbf{0.415} \\
% \bottomrule
% \end{tabular}
% \label{table:metrics_ecoli}
% \end{table}

% pca metrics:
% \multicolumn{4}{c}{\textbf{Eggs}}\\
% \midrule
% Metric & Raw & Low-pass filtering & Enhancement\\
% \midrule
% silhouette            & 0.512 & 0.576 & \textbf{0.601} \\
% homogeneity           &  0.471 & \textbf{0.712} &  0.681 \\
% completeness          &  0.364 &  0.547 &  \textbf{0.564 }\\
% adjusted rand index    &  0.197 & 0.391 & \textbf{0.432} \\

\section{Discussion and Limitations}
\label{sec:discus}

Results presented in \Cref{sec:results} attest the benefits of the proposed filtering
schemes when used in combination with visualization methods.
%, revealing the positive impact of graph filtering as a tool to assist visualizations.
The provided qualitative and quantitative results shows that
filtering mechanisms can be primordial in the context of visualization.

It is important to make clear we are not advocating that filtered data should replace original data in all
visualizations. Rather, we encourage the concomitant use of ``raw'' and filtered data 
in visualizations. A good example is the synthetic dataset visualization
depicted in \Cref{fig:blobs_splom_original,fig:blobs_splom_low,fig:blobs_splom_enh}, where the plots from raw and filtered data bring complementary 
information. In fact, scatter plots of the original data
discriminate the groups quite well as to the first two attributes, while the histograms from filtered data
shows precisely where those attributes are more concentrated.

We consider the proposed methodology just a first step towards incorporating signal processing tools
into information visualization methods, as a multitude of possibilities are still to be explored.
For instance, how to design optimal filters for a particular application? 
Which filter is more appropriate for a given visualization method? Is it possible
to learn optimal filters from existing visualizations? Answering those questions
is not straightforward, demanding a substantial amount of investigation as future work.

There are, though, some pitfalls of our methodology, as for example
the proper definition of the graph structure from which the whole graph signal analysis is derived. 
As we discussed in \Cref{sec:method}, the topology of the graph impacts directly
in the result of the spectral filtering process. 
A graph with a dense set of edges connecting unrelated nodes can lead to unsatisfactory results. 
This problem raises the issue of how to generate graphs that are more appropriate than the KNN-graph used in our methodology. 
An option could be to adapt graph learning methods~\cite{dong2016learning} to the context
of GSP-assisted data visualization.

% ------

% Fourier filters are great for many types of data analysis, but they are very
% nonlocal, i

% ------

Another import aspect in the present context is that GSP is not the only
alternative to handle data towards improving visualizations. 
Other schemes such as spatial and statistical filtering, widely used in graphics and computer vision,
could also be adapted to visualization purposes.

%-------------------------------------------------------------------------
\section{Final Considerations}

In this chapter we introduced a new filtering methodology based on graph signal processing theory,
which turns out to be useful to improve the quality of visualizations.
The provided qualitative and quantitative results attest the positive impact of the 
proposed filtering method to generate high quality visualizations.
We believe that present work is just a first step towards making filtering mechanisms a 
basic tool in the context of visualizations, opening a new avenue for further developments.
