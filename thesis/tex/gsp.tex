%!TEX root=../thesis.tex

Graphs naturally describe the relational structure of data in many scenarios, including data on social, brain, and transportation networks (\Cref{fig:wGraphs}).
Each edge of the graph can have an associated value, usually called weight, representing, for instance, geographical proximity or similarity between nodes connected by such edge.
Such graphs are denominated \emph{weighted graphs}. 
Moreover, depending on whether edges have or not a direction, graphs are referred as directed or undirected graphs respectively.
In this work, we only consider undirected weighted graphs.

\begin{figure}
\centering
 \begin{tikzpicture}
    \node[draw=none,shade,
      top color=white,
      bottom color=white,
      rounded corners=2pt,
      minimum width=12cm,minimum height=5cm,
      inner sep=0,
      blur shadow={shadow blur steps=3}
    ]{};
    \node at (-3.9,-.4)[draw=none]  
       {\includegraphics[width=.18\linewidth]{figs/intro/mynet_f.pdf}};
    \node at (0,-.4)[draw=none]  
       {\includegraphics[width=.3\linewidth]{figs/intro/brain_net.pdf}};
    \node at (4.1,-.4)[draw=none]  
       {\includegraphics[width=.16\linewidth]{figs/preliminares/graph_NY.png}};  
    \node[text width=3cm,,align=center,inner sep=0,color=mygreen] at (-3.7, 1.6) {\small \textsf{Social network}};
    \node[text width=3cm,inner sep=0,color=mygreen] at (0.5, 1.6) {\small \textsf{Brain network}};
    \node[text width=4cm,align=center,inner sep=0,color=mygreen] at (3.9,1.6) {\small \textsf{Transportation network}};
  \end{tikzpicture}
\caption{A few examples of graphs that describe relational structure of data.}
\label{fig:wGraphs}
\end{figure}

Formally, a graph $\Gg=\{\vset,\eset\}$ is defined by a set of nodes $\vset = \{\n 1, \n 2, \ldots, \n \gsize\}$ and a set of edges $\eset$.
In an undirected graph, an edge is an unordered pair of nodes. 
If there exists an edge connecting nodes $\n i$ and $\n j$, i.e. $(\tau_i,\tau_j)\in
E$, those nodes are said to be adjacent or neighbors.
We can represent a graph $\Gg$ using its adjacency matrix $\adj$. The entries $a_{ij}$ of this matrix are equal to one if and only if $(\tau_i,\tau_j)\in E$. 


Weighted graphs can be represented by a triplet $\Gg^{w}=\{\vset,\eset,w\}$, 
where in addition to $\vset$ and $\eset$, there is 
a weight function $w: E \rightarrow \rset$ that associates a non-negative 
scalar to each edge of $G$. 
The \emph{weighted adjacency matrix} of $G$, denoted as 
$W=(w_{ij})$, is the matrix satisfying $w_{ij}=w(\tau_i,\tau_j)$ if ${\n i}$ is adjacent to ${\n j}$
and $w_{ij}=0$ otherwise. 
This entries represent the weight of the edges between nodes.
By construction, $\wei$ is a symmetric square matrix of order $\gsize$. 
In our context, besides of assuming an undirected weighted graph, $G$ is assumed to be \emph{connected}, that is, for every pair of nodes there is a sequence of adjacent edges connecting the nodes.


A signal defined on the nodes of $G$, $f:V\rightarrow\rset$, is a function
that associates a real value $f(\tau_i)$ to each node $\tau_i\in V$. 
For example, left column in \Cref{fig:pathgraph} shows some signals defined in
a path graph with 64 nodes, in which each node, except the two extreme ones, is adjacent to exactly two
other nodes (top in \Cref{fig:pathgraph}).
In \Cref{fig:pathgraph} the amplitude of the signal is represented using a double encoding:
the height of the vertical blue lines and the color of the node.


\section{Spectral graph theory}
\label{sec:fundamentacao_teg}

%Along with the weight matrix, a graph can be represented by its adjacency matrix, normalized or unnormalized Laplacian matrix, among others.
%Representing a graph by a matrix allows for several algebraic computations. 
%In particular, it is possible to analyze a graph by the spectral decomposition (eigenvalues and eigenvectors) of these matrices.


\begin{figure}
  \centering
  \shadowimage[width=0.98\textwidth]{figs/gsp/path_graph.png}\\[.3cm]
  \caption[Signals defined on an path graph with 64 nodes and its respective GFT.]{\textbf{Signal:} eigenvectors $u_1$, $u_2$, $u_3$, $u_{10}$, $u_{32}$, $u_{63}$, and $f_0$ signals
    defined on a path graph with 64 nodes. Signal $f_0$ is a combination
    of the low ($u_3$) and high frequency ($u_{63}$) signals. Circles represent the graph
    nodes, and the height of the vertical blue lines and the color of the nodes correspond to the
    amplitude of the signal. \textbf{GFT:} The GFT are indicated by arrows.
    The GFT corresponding to the signal $f_0$ indicates the presence of low
    and high frequencies.  
    % The yellow curve illustrates the DFT of $f_0$ when defined on the real line. 
    % Adapted from \cite{dal2017wavelet}. 
    \label{fig:pathgraph}}
\end{figure}

Spectral graph theory is the study of graph properties by means of algebraic computations from a graph matrix.
Such matrix could be the adjacency or the Laplacian matrix, among others. 
The graph spectrum and its applications have been extensively studied, and are well documented in several surveys and books \cite{cvetkovic2009,chung1997spectral,biyikoglu2007laplacian}. 


In this work, we are specially interested in the graph Laplacian spectrum because it provides with the means of naturally extending the Fourier transform to the context of graphs, 
allowing the definition of signal processing operations on graphs, including the Wavelets transform.


For a real valued function $f: \vset \rightarrow \rset$, the graph Laplace operator  for graphs is defined as  $(\Delta f)(i) = \sum_{\isneigh{\n i}{\n j}} w_{ij}[f(i)-f(j)]$.
This is an analog of the continuous Laplace operator, which clearly can be seen in a grid graph, where applying the operator to any non-boundary node leads to:
\begin{equation}
\begin{split}
-(\Delta f)(i) =& \left[f(x_0+1,y_0)-f(x_0,y_0)\right] - \left[f(x_0,y_0)-f(x_0-1,y_0)\right]\\
&+ \left[f(x_0,y_0+1)-f(x_0,y_0)\right] - \left[f(x_0,y_0)-f(x_0,y_0-1)\right] \\
\approx &\frac{\partial^2f}{\partial x^2}(x_0,y_0)+\frac{\partial^2 f}{\partial y^2}(x_0,y_0) = (\Delta f)(x_0,y_0),
\end{split}
\end{equation}
which is the Laplacian finite difference approximation called \emph{five-point stencil}.
 



Using matrix notation the Laplacian can be defined as:
\begin{equation}
\lapun := \degm - \wei,
\end{equation}
where
$D=\text{diag}(d_0,d_2,\ldots,d_{n-1})$ is a diagonal matrix with entries
$d_i=\sum_j w_{ij}$ and $\gsize$ is the number of nodes in $V$.  
The graph Laplacian is a real, symmetric, and
positive-semidefinite matrix, what ensures a complete set of
orthonormal real eigenvectors $\{\eigvec_\ell\}_{ \ell=1,\dots,{\gsize-1}}$, 
with corresponding non-negative
real eigenvalues $\Lambda = \{\eigval_\ell\}_{ \ell=1,\dots,{\gsize-1}}$. 
Assuming a connected graph, eigenvalues can be ordered $0 = \eigval_1 < \eigval_2\leq\dots\leq\eigval_{\gsize}:=\eigval_{\text{max}}$.
Moreover, zero is always an eigenvalue of $L$ whose corresponding 
eigenvector is a constant vector.


The graph
Laplacian eigenvectors $u_\ell:V\rightarrow\rset$ can be interpreted as
signals on the graph $G$. 
The left column in \Cref{fig:pathgraph}, from top to bottom, shows the eigenvectors 
$u_1$, $u_2$, $u_3$, $u_{10}$, $u_{32}$, $u_{63}$ of the graph Laplacian for a path
graph with 64 nodes.
It can be seen that eigenvectors associated to 
small eigenvalues tend to have a less oscillatory behavior than eigenvectors associated 
to large eigenvalues. 
Therefore, the eigenvalues and eigenvectors of the graph Laplacian play a similar role as 
frequencies and basis functions in the classical Fourier theory. 
More specifically, small eigenvalues, that is, the ones closer to the eigenvalue zero, correspond to low frequencies,
while large eigenvalues correspond to high frequencies. 
A more detailed discussion about the relation between the spectrum of 
Laplacian matrices and Fourier theory can be found in the work by~\citeonline{shuman2016vertex}.

% The \emph{zero crossings set} of $f$ on $G$ is given by:

% \begin{equation}
%   Z_G(f)=\{(\tau_i,\tau_j)\in E\suchthat f(\tau_i)f(\tau_j)<0\}. 
% \end{equation}

% \noindent $Z_G(f)$ is the set of edges connecting nodes where $f$ has
% different signs (positive and negative). The larger the value
% $|Z_G(f)|$, the more $f$ changes its sign across the graph.
% \Cref{fig:zero_crossings} shows the number of zero crossings of
% the graph Laplacian eigenvectors $u_{\ell}$ as $\ell$ grows, assuming
% the eigenvectors are ordered in non-decreasing order of eigenvalues,
% for a path graph with 64 nodes. 


% \begin{figure}
%   \centering
%   \shadowimage[width=0.39\textwidth]{figs/wavs/cise/zero_crossings.pdf}\\[.3cm]
%   \caption{Number of zero crossings, $|Z_G(u_{\ell})|$, of the graph
%   Laplacian eigenvectors $u_{\ell}$, $\ell=1,2,\ldots,64$ for a path
%   graph with 64 nodes. Source: \cite{dal2017wavelet}. \label{fig:zero_crossings}}
% \end{figure}



\emph{The normalized Laplacian matrix} is defined as:
\begin{equation}
\lapsn:=\degm^{-\frac{1}{2}}\lapun\degm^{-\frac{1}{2}}.
\end{equation}
Such matrix also has $\gsize$ real non-negative eigenvalues  $\{\eigvaln_\ell\}_{ l=1,\dots,{\gsize}}$ and a complete set of orthonormal eigenvectors $\{\eigvecn_\ell\}_{ l=1,\dots,{\gsize}}$.
Moreover, on a connected graph the eigenvalues of $\lapsn$ are bounded: $0=\eigvaln_1<\eigvaln_2\leq\dots\leq\eigvaln_{\gsize}:=\eigvaln_{\max}\leq2,$ with $\eigvaln_{\max} = 2$ if and only if the graph is bipartite.

Non-normalized and normalized Laplacian matrices, along with the adjacency matrix, can be used to extend the classical Fourier theory to graphs.
In this work we will use both the non-normalized and normalized Laplacian matrices.
More details on using the adjacency matrix to extend the Fourier theory can be found in the work by \citeonline{Sandryhaila2014}.


\section{Graph Fourier transform}
\label{sec:fundamentacao_tfg}
%A definição da transformada de Fourier em grafos, tem sido inspirada manifolds!! no enucuentras la referencia =(
The classic Fourier transform is defined as the expansion of a function $f$  in terms of complex exponentials:
\begin{equation}
\fourier{f}(\xi)  =  \langle f,e^{2\pi i\xi t} \rangle  = \int_\rset f(t)e^{-2\pi i\xi t} dt,
\end{equation}
where $\xi$ stands for the frequency and $t$ for the time. Such complex exponentials are eigenfunctions of the Laplacian operator, in one dimension:
\begin{equation}
-\Delta(e^{2\pi i\xi t}) = -\frac{\partial^2}{\partial t^2}e^{2\pi i\xi t}  = (2\pi\xi)^2 e^{2\pi i\xi t}.
\end{equation}

By analogy to the continuous Laplacian operator, where eigenfunctions and eigenvalues are the Fourier modes and frequencies respectively, the $\eigvec_\ell$ and $\eigval_\ell$, $\ell=0,\ldots,n-1$ are considered as graph Fourier modes and frequencies. 
Therefore, small values of $\eigval_\ell$ correspond to low frequency modes while large values indicate high frequency modes. 
The \emph{graph Fourier transform}  of $\vf$ at the frequency $\lambda_\ell$ is defined as \cite{Hammond2011}:

\begin{equation}
% \fourier{f}(\eigval_\ell)  :=  \langle \vf,\eigvec_\ell \rangle  = \sum_{i\in\vset} f(i)\eigvecs_\ell (i),
  \hat{f}(\lambda_\ell) = \langle u_\ell,f \rangle = \displaystyle\sum_{j=1}^{n} u_\ell(\tau_j)f(\tau_j),
  \label{eq:gft}
\end{equation}
where $\eigval_\ell$ is an eigenvalue of $\lapun$, $\eigvec_\ell$ is its corresponding eigenvector and $\eigvec_\ell(\tau_j)$ is the value of the eigenvalue at the position $j$. 
Since the GFT assigns a scalar value to each eigenvalue (frequency) 
$\lambda_\ell\in\Lambda$, one can visualize the result of a GFT by plotting the pairs
$(\lambda_\ell,\hat{f}(\lambda_\ell))$, $i=0,1,\ldots,n-1$, using a bar-like plot 
as illustrated on the right of \Cref{fig:pathgraph}. 

   

% A signal defined on the nodes of $G$ is a
% function $f:V\rightarrow\rset$ that associates a scalar $f(\tau_i)$ to each node $\tau_i\in V$.  

% The \emph{Graph Fourier Transform} (GFT) of a signal $f$, denoted
% $\hat{f}:\Lambda\rightarrow\rset$, where $\Lambda$ is the spectral domain (set of eigenvalues), is defined as:
% \begin{equation}
%   \hat{f}(\lambda_\ell) = \langle u_\ell,f \rangle = \displaystyle\sum_{j=1}^{n} u_\ell(\tau_j)f(\tau_j),
%   \label{eq:gft}
% \end{equation}
% \noindent 



\begin{figure}
\centering
\shadowimage[width=\linewidth]{figs/gsp/irregular_graph.png}
\\[.3cm]
\caption[Signals defined on an irregular graph and its respective GFT.]{Signals defined on an irregular graph and its respective GFT.
In every case, the signal is encoded using the color of each node and its respective 
GFT is shown on bottom of each graph.
The two first rows show eigenvectors $u_1$, $u_2$, $u_{11}$, $u_{211}$, $u_{380}$, $u_{399}$ signals
defined on an irregular graph with 400 nodes. 
$f_0$ is a signal defined as a Gaussian function centered in a given node.
$f1$ and $f2$ are signals defined to show the behavior of the GFT in other scenarios.}\label{fig:gft}
\end{figure}


% Usually, the GFT is represented by a plot formed by the points
% $(\lambda_\ell,\hat{f}(\lambda_\ell))$, $i=1,2,\ldots,n$.  
Similarly
to the classic Fourier transform, the GFT reveals the frequencies
present in a signal.  High frequency coefficients
$\hat{f}(\lambda_\ell)$ (when $\lambda_\ell$ is in the rightmost part
of the spectrum) indicate that a signal varies abruptly in some
regions of the graph, while low frequency coefficients suggest smooth
signal variation.  In order to illustrate this concept and show the
relation with classic Fourier transform, we consider the path graph
with 64 nodes of \Cref{fig:pathgraph}. We consider seven eigenvectors as signals,
four of low frequencies ($u_1$, $u_2$, $u_3$, $u_{10}$), one mid-frequency ($u_{32}$)
 and one of high frequency ($u_{63}$, as illustrated in
\Cref{fig:pathgraph}. The GFT of those signals is also depicted on the right of
\Cref{fig:pathgraph}, corresponding to the spikes on the corresponding
frequencies, the same behavior as in the classic Fourier transform.

The GFT provides a global description of the signal behavior on the
graph.  For example, let $f_0$ be a signal defined as a combination of one
low and high frequency eigenvectors ($u_3$ and $u_{63}$ respectively), as illustrated on
the bottom left of \Cref{fig:pathgraph}.  The GFT of $f_0$,  shown on the bottom right of \Cref{fig:pathgraph},
indicates the presence of low and high frequencies in the signal,
suggesting that the signal has both smooth and abrupt variation. 
% The
% yellow curve illustrates the magnitude of the conventional \emph{Discrete
% Fourier Transform} (DFT) of $f_0$ when defined on the real line.

In \Cref{fig:gft} we illustrate the GFT of some signals defined on an irregular graph with 400 nodes,
colors indicate the values of the signal in each node.
The first two rows of \Cref{fig:gft} show the GFT of six eigenvectors as signals on the graph:
three low frequencies ($u_1$, $u_2$, $u_{11}$), one mid-frequencies ($u_{211}$)
and two high frequencies ($u_{380}$ and $u_{399}$). 
$f_0$ (bottom left of \Cref{fig:gft}) shows the GFT of a Gaussian function on the same irregular graph. 
As depicted on the bottom left of \Cref{fig:gft}, the largest coefficients of the GFT of $f_0$ are 
concentrated in the low frequency region of the spectrum (smaller eigenvalues). 
In other words, the smooth global variation of this function is captured by the GFT. 
On the bottom of \Cref{fig:gft}, we show other signals, $f_1$ and $f_2$ to illustrate the 
behavior of the GFT in different scenarios. 
Most regions of $f_2$ have adjacent nodes with the same value, except regions on the ``border'' of the ``V'', which is reflected in larger values on the low frequencies.
On the other hand, $f_1$  has only some nodes with different value from its neighbors, which can be
seen in the presence of all kind of frequencies in the spectrum domain.


Under certain conditions, the classic inverse Fourier transform is given by:
\begin{equation}
f(t) = \int_\rset \fourier{f}(\xi) e^{2\pi i\xi t} d\xi,
\end{equation}
and its corresponding \emph{inverse Graph Fourier Transform} (iGFT) is defined as:
\begin{equation}
% f(i) = \sum_{\ell=0}^{\gsize-1} \fourier{f}(\eigval_\ell)\eigvecs_\ell(i).
f = \displaystyle\sum_{\ell=1}^{n} \hat{f}(\lambda_\ell)u_\ell
\end{equation}

% Given the GFT $\hat{f}$, the original signal $f$ can be recovered via the 
% \textit{inverse Graph Fourier Transform} (iGFT), which is defined as:
% \begin{equation}
% f = \displaystyle\sum_{\ell=1}^{n} \hat{f}(\lambda_\ell)u_\ell
% \label{eq:igft}
% \end{equation}
If we denote by $U$ the (orthogonal) matrix with columns given by the eigenvectors $u_\ell$,
the GFT and iGFT can be obtained by matrix multiplication as follows:
\begin{eqnarray}
\fbox{\mbox{GFT}}\,\,\, & \qquad & \,\,\,\fbox{\mbox{iGFT}}\nonumber\\
\hat{f} = U^{\!\top}f & \qquad & f = U\hat{f} 
\label{eq:mgf}
\end{eqnarray}

\section{Spectral Filtering}
\label{sec:spec_filt}

A \emph{graph spectral filter}, or kernel, 
$\hat{h}:\Lambda\rightarrow\rset$ is a function defined in the
spectral domain that associates a scalar value $\hat{h}(\lambda_\ell)$
to each eigenvalue $\lambda_\ell\in\Lambda$. 
The GFT $\hat{f}:\Lambda\rightarrow\rset$ can be seen as a particular
instance of a graph spectral filter.  

A \emph{graph spectral filtering} of a signal $f$ is defined as:
\begin{equation}
\hat{\tilde{f}} = \hat{h}\,\hat{f}
\label{eq:gsf}
\end{equation}
where $\hat{f}$ is the GFT of $f$ and $\hat{h}$ a graph spectral filter.
The filtered version $\tilde{f}$ of $f$ in the graph domain is
\begin{equation}
  \tilde{f}(\tau_j)=\sum_{\ell=1}^{n} \hat{h}(\lambda_{\ell})\widehat{f}(\lambda_{\ell})u_{\ell}(\tau_j)
  \label{eq:gfe}
\end{equation}
Using the matrix notation defined in Equation~\ref{eq:mgf} after some algebraic manipulation 
one can obtain :
\begin{equation}
\tilde{f}=UHU^{\!\top}f
\label{eq:gf}
\end{equation}
where $H$ is a diagonal matrix with entries $\hat{h}(\lambda_1),\ldots,\hat{h}(\lambda_n)$.

The design of a proper filter $\hat{h}$ is application dependent, some examples can be found in the work by~\citeonline{Shuman2013}. In this work we will 
use low-pass $\hat{h}_l$, high-pass $\hat{h}_h$, 
and enhancement $\hat{h}_e$ filters, which are defined, among many possibilities, as: 
\begin{eqnarray}
\hat{h}_l(\lambda) & = & \exp \left(\frac{(-\alpha\,\lambda)^2}{ (2\,\lambda_{\max})^2}\right),  \qquad\, \alpha>0\\
\hat{h}_h(\lambda) & = & 1 - \hat{h}_l(\lambda) \\
\hat{h}_e(\lambda) & = & \beta f_l(x) + (1-\beta) f_h(x), \quad \beta\in[0,1]
\end{eqnarray}
where $\lambda_{\max}$ is the largest eigenvalue and $\alpha,\, \beta$ are user defined parameters.

\Cref{fig:filters} illustrates the effect of a low-pass filter and an enhancement when
applied in a noisy step function.

\begin{figure}
\centering
\includegraphics[width=0.45\linewidth]{figs/gsp_filtering/step_function.pdf}\qquad
\includegraphics[width=0.45\linewidth]{figs/gsp_filtering/step_function_noisy.pdf}\\
\hspace*{-0.3cm}\includegraphics[width=0.41\linewidth]{figs/gsp_filtering/step_low_filter.pdf}\qquad
\includegraphics[width=0.41\linewidth]{figs/gsp_filtering/step_enh_filter.pdf}\\
\includegraphics[width=0.45\linewidth]{figs/gsp_filtering/step_low.pdf}\quad
\includegraphics[width=0.45\linewidth]{figs/gsp_filtering/step_enh.pdf}
\caption[Low-pass and enhancement filtering of a noisy step function.]{Top: Step function (left) and its noisy version (right). 
Middle: Low-pass (left) and Enhancement (right) filters.
Bottom: Noisy step function filtered with the low-pass (left) and enhancement (right) filters.}
\label{fig:filters}
\end{figure}


