import numpy as np
from skimage.draw import line_aa, line
from skimage.transform import hough_line
from sklearn.preprocessing import MinMaxScaler


def dot(vA, vB):
    return vA[0]*vB[0]+vA[1]*vB[1]
def ang(lineA, lineB):
    # Get nicer vector form
    vA = [(lineA[0][0]-lineA[1][0]), (lineA[0][1]-lineA[1][1])]
    vB = [(lineB[0][0]-lineB[1][0]), (lineB[0][1]-lineB[1][1])]
    # Get dot prod
    dot_prod = dot(vA, vB)
    # Get magnitudes
    magA = dot(vA, vA)**0.5
    magB = dot(vB, vB)**0.5
    # Get cosine value
    cos_ = dot_prod/magA/magB
    # Get angle in radians and then convert to degrees
    angle = np.arccos(dot_prod/magB/magA)
    # Basically doing angle <- angle mod 360
    ang_deg = np.degrees(angle)%360

    if ang_deg-180>=0:
        # As in if statement
        return 360 - ang_deg
    else: 

        return ang_deg
    
def pc_metrics(data):
    size = 512
    scaler = MinMaxScaler((0, size-1))
    sc_data = np.floor(scaler.fit_transform(data))

    n, ncols = data.shape
    imgs = [None]*(ncols-1)
    bins = [None]*(ncols)
    R = [None]*(ncols-1)
    L = [None]*(ncols-1)
    D = [None]*(ncols-1)
    H = [None]*(ncols-1)

    for j in range(ncols-1):
        img = np.zeros((size, size), dtype=np.float64)
        img_ = np.zeros((size, size), dtype=np.float64)
        R[j] =np.zeros(n) 
        L[j] =np.zeros(n) 
        D[j] =np.zeros(n) 
        for i in range(n):
            y = sc_data[i, j:j+2]
            rr, cc = line(int(y[0]), 0, int(y[1]), size-1)
            img_[rr, cc] += 1
            R[j][i] = y[0]
            L[j][i] = y[1]
            D[j][i] = y[1] - y[0]
            rr, cc, v = line_aa(int(y[0]), 0, int(y[1]), size-1)
            img[rr, cc] += v
        bins[j] = img_[:,0]
        imgs[j] = img

    bins[ncols-1] = img_[:,-1]
        
    for c in range(ncols-1):
        H[c] = np.zeros((size, size), dtype=np.float64)
    #     for i in range(size):
    #         for j in range(size):
    #             H[c][i,j] += (bins[c][i]>0 and bins[c+1][j]>0)

    #     plt.imshow(H[c])
    #     plt.show()
        H[c] = bins[c][np.newaxis].T * bins[c+1][np.newaxis]
    #     plt.imshow(bins[c][np.newaxis].T*bins[c+1][np.newaxis])
    #     plt.show()


    a_l_crossing = [None]*(ncols-1)
    a_median_angles = [None]*(ncols-1)
    a_q50 = [None]*(ncols-1)
    a_p_norm = [None]*(ncols-1)
    a_mutual = [None]*(ncols-1)
    a_entropy = [None]*(ncols-1)
    n = R[0].shape[0]
    for c in range(ncols-1):
        l_crossing = 0
        angles = []
        for i in range(n):
            for j in range(n):
                if (R[c][i] < R[c][j] and L[c][i] > L[c][j]) or (R[c][i] > R[c][j] and L[c][j] < L[c][j]):
                    l_crossing += 1
                    a = ang([[0, R[c][i]], [size, L[c][i]]], [[0, R[c][j]], [size, L[c][j]]])
                    angles.append(a)

        mutual = 0
        convergence = 0
        divergence = 0
        for ri in range(size):
            for rj in range(size):
                pij = H[c][ri, rj]/size
                pi = bins[c][ri]/size
                pj = bins[c][rj]/size    
                if  pij > 0 and pi >0  and pj > 0:
                    m = pij * np.log(pij/(pi*pj))
                    mutual += m

        l_c_normalized = 2*l_crossing/(size*(size-1))
        median_angles = np.nanmedian(angles)
        par_nor = D[c]/size
        q75 = np.nanpercentile(par_nor, 75)
        q25 = np.nanpercentile(par_nor, 25)
        q50 = np.nanpercentile(D[c], 50)
        p_norm = 1-abs(q75-q25)

        a_l_crossing[c] = l_c_normalized
        a_median_angles[c] = (median_angles)
        a_q50[c] = q50
        a_p_norm[c]  =p_norm
        
        nbins = 256
        h_bins, _ = np.histogram(imgs[c], nbins)
        p_bins = h_bins/(size*size)
        p_bins[p_bins==0] = 0.5/(size*size)
        entropy_bins = np.sum(p_bins*np.log(1/p_bins))
        
        a_l_crossing[c] = l_c_normalized
        a_median_angles[c] = median_angles
        a_q50[c] = q50
        a_p_norm[c] =p_norm
        a_mutual[c] = mutual
        a_entropy[c] = entropy_bins

        
    metrics_ = {
        'l_crossing': np.mean(a_l_crossing),
        'median_angles': np.mean(a_l_crossing),
        'median dir': np.mean(a_q50),
        'mutual': np.mean(a_mutual),
        'pixel entropy': np.mean(a_entropy)
    }
    return metrics_