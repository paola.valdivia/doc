# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 11:46:39 2015

@author: pao
"""

import numpy as np
import networkx as nx
from wavelets.filters import filter_bank_design


#%%
def waviz_all(G, fp, nScales=3, U = None, D=None):     
    if U == None:
        D, U = eig(G)
    Wp_st = wavelet_analysis_st (fp, G, U, D, nScales)
    
    Wp_s = wavelet_analysis_s (fp, G, U, D, nScales)    
    
    Wp_t = wavelet_analysis_t (fp, G, nScales)    
    
    return Wp_st, Wp_s, Wp_t
    
#%%
def wavelet_analysis_st(fp, G, U=None, D=None, nScales=4, designtype='abspline3'):
    if U == None:
        D, U = eig(G)
    N,n = fp.shape   
#    weight_temp = mean(B)  
    T =   nx.path_graph(n)
    DT, UT = eig(T)
       
    Dn, IDn = nDCopies(D, DT)
#    IDn = int32(IDn);    
    print ('Designing transform in spectral domain')
    lmax = np.max(Dn)
    g =  filter_bank_design(lmax, nScales, designtype=designtype)
    
    print ('Wavelet transform\n')

    Wp = np.zeros( (nScales+1, N*n))
    f = fp.ravel()
    
    U = U.T
    UT = UT.T
    
    for j in range(nScales+1):
        print (j)
        Wp[j]  =  gwft_iter( f, g[j], U, UT, Dn, IDn )
    return Wp
    
def wavelet_analysis_t(fp, G, nScales, designtype='meyer'):
    N,n = fp.shape   
#    weight_temp = mean(B)  
    T =   nx.path_graph(n)
    DT, UT = eig(T)
       
#    IDn = int32(IDn);    
    print ('Designing transform in spectral domain')
    lmax = np.max(DT)
    g = filter_bank_design(lmax, nScales, designtype='abspline3')
    
    print ('Wavelet transform\n')

    Wp = np.zeros( (nScales+1, N*n))
    
    UTt = UT.T
    
    Wp = np.zeros( (nScales+1, N*n))
    for j in range(nScales+1):
        gl = g[j](DT)
        r = np.zeros( (N, n) )
        for ni in range(N):
            fhat = np.dot(UTt, fp[ni])
            r[ni] = np.dot(UT, (fhat*gl))
        Wp[j] = r.ravel()
    return Wp
    
def wavelet_analysis_s(fp, G, U, D, nScales, designtype='meyer'):
    N,n = fp.shape   

    print ('Designing transform in spectral domain')
    lmax = np.max(D)
    g = filter_bank_design(lmax, nScales, designtype='abspline3')    
    Ut = U.T
    print ('Wavelet transform\n')

    Wp = np.zeros( (nScales+1, N*n))
    for j in range(nScales+1):
        gl = g[j](D)
        r = np.zeros( (n, N) )
        for ti in range(n):
            fhat= np.dot(Ut, fp[:,ti])
            r[ti] = np.dot(U, (fhat*gl))
        Wp[j] = r.ravel(order='F')
    return Wp

    
#%%
def eig(G):
    L = nx.laplacian_matrix(G)
    D, U = np.linalg.eig(L.toarray())
    idx = D.argsort() 
    D = D[idx]
    U = U[:,idx]
    return D, U
    
def nDCopies(D, DT):
     Dn = kron_sum(D, DT)
     IDn = np.argsort(Dn)
     Dn = Dn[IDn]
     return Dn, IDn

def kron_sum(A, B):
    tmp = A + B[np.newaxis].T
    return tmp.ravel()
    
#%%

def gwft( f, U ):    
    fhat= np.dot(U.T,f);    
    return fhat
    
def gwft_iter( f, g, U, UT, Dn, IDn ):    
    gtl = g(Dn)
    r = np.zeros(Dn.shape)
    for i in (gtl>0.00001).nonzero()[0]:
        Ui = iEig(IDn[i], U, UT)
        fhati = np.dot(Ui, f)
        r += Ui*(gtl[i]*fhati)
#    % fhat=V'*f;    
#    % r=V*(fhat.*g(t*lambda))
    return r
    
def iEig(i, U, UT):    
    N = U.shape[0] 
    n = UT.shape[0]    
    J, I = ind2sub([n, N], i)
    iUn = np.kron(U[I], UT[J])
    
    return iUn
    
def ind2sub(array_shape, ind):
    rows = (ind.astype('int') / array_shape[1])
    cols = (ind.astype('int') % array_shape[1]) # or numpy.mod(ind.astype('int'), array_shape[1])
    return (rows, cols)