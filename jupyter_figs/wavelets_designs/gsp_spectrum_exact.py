from __future__ import division

import numpy as np

def gsp_spectrum_exact(x, num_pts=None):
    
    N = len(x)
    
    if type(num_pts) is int:
        n = num_pts
        x_spectrum = np.zeros((n,))
        y_spectrum = np.zeros((n,))
        ind = 0
        
        
        selected = range(0,N-1,num_pts+1)
        selected.append(N-1)
        if len(selected) > num_pts:
            del selected[-2]
            
        for l in selected:
                x_spectrum[ind] = x[l]
                y_spectrum[ind] = l/float(N-1)
                ind += 1
        
    else:
        markers = np.ones((N,))
        
        for i in range(1,N):
            if np.abs(x[i] - x[i-1])<(1e-6):
                markers[i-1] = 0
                
        n = int(np.sum(markers))
        x_spectrum = np.zeros((n,))
        y_spectrum = np.zeros((n,))
        ind = 0
        
        for l in range(N):
            if markers[l] == 1:
                x_spectrum[ind] = x[l]
                y_spectrum[ind] = l/float(N-1)
                ind += 1
    
    approx_spectrum = dict()
    approx_spectrum['x'] = x_spectrum
    approx_spectrum['y'] = y_spectrum
    
    return(approx_spectrum, x_spectrum)