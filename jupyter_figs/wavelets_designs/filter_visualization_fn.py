from __future__ import division

import numpy as np
import matplotlib.pyplot as plt

def filter_visualization_fn(G, filters, num_filters, filter_type):
    
    L = G['L']
    lmax = G['lmax']
    
    x_spectrum = np.linalg.eigvalsh(L.toarray())
    
    x = np.arange(0, lmax-0.0001, 0.01)
    n = len(x)
    
    # Energy function G
    G = np.zeros((n,))
    for i in range(len(x)):
        for j in range(num_filters):
            G[i] += filters[j](x[i]) ** 2
            
    z = np.zeros((n,))
    ymax = 0
    
    plt.figure()
    for j in range(num_filters):
        for l in range(n):
            z[l] = filters[j](x[l])
            
        if ymax < np.max(z):
            ymax = np.max(z)
            
        if (num_filters) == 8 and (filter_type == 'warped_translates_log'):
            if j == 0:
                plt.plot(x,z,'b')
            if j == 2:
                plt.plot(x,z,'g')
            if j == 3:
                plt.plot(x,z,'r')
            if j == 4:
                plt.plot(x,z,'c')
            if j == 5:
                plt.plot(x,z,'m')
            if j == 6:
                plt.plot(x,z,'y')
            if j == 7:
                plt.plot(x,z,'k')
            if j == 1:
                plt.plot(x,z,'b')
        else:                
            plt.plot(x,z)
        
    xmax = x_spectrum[-1]
    #ymax = np.max(G)    
    
    plt.xlim(xmin=-0.1, xmax=xmax+0.1)
    plt.ylim(ymin=-0.1, ymax=ymax+0.1)
    #plt.plot(x,G,'k-')
    
    plt.plot(x_spectrum,np.zeros(len(x_spectrum)),'kx', mew=1.5)
        
    plt.grid('off')
    plt.show()
    
    return()